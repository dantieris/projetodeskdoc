package model;

public class Usuario {
    private String login;
    private String senha;

    public Usuario(String usuario, String senha) {
        this.login = usuario;
        this.senha = senha;
    }
    
    public Usuario() {}

    /**
     * @return the usuario
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the usuario to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * @return the senha
     */
    public String getSenha() {
        return senha;
    }

    /**
     * @param senha the senha to set
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    
    
}
