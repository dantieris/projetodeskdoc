package deskdoc.servidor;

import model.ITexto;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Texto extends UnicastRemoteObject implements ITexto{
    String texto = "";
    
    public Texto(String texto) throws RemoteException {
        this.texto = texto;
    }
    
    public Texto() throws RemoteException {}
    
    @Override
    public void setTexto(String texto) throws RemoteException {
        this.texto = texto;
    }

    @Override
    public String getTexto() throws RemoteException {
        return this.texto;
    }
    
}
