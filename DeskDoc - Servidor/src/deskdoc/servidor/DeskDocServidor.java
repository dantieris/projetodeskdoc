package deskdoc.servidor;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import javax.swing.JOptionPane;
import model.Usuario;

public class DeskDocServidor {

    List<String> documentos;
    List<Usuario> usuarios;

    public DeskDocServidor() {
        this.documentos = new ArrayList<>();
        this.usuarios = new ArrayList<>();
        
        ServerSocket server = null;
        try {
            // Cria o socket do servidor, esperando conexão pela porta 5000.
            server = new ServerSocket(5000);

            // Criando o rmi registry
            LocateRegistry.createRegistry(1099);

            while (true) {
                System.out.println("Aguardando conexão...");
                // Pega uma conexão do cliente e cria um socket.
                Socket socket = server.accept();

                System.out.println("Cliente conectado!");

                // Cria uma thread para tratar as solicitações do cliente.
                new Thread(new EscutaCliente(socket)).start();
            }
        } catch (IOException ex) {
            try {
                // Fecha o socket caso ocorra uma exceção
                server.close();
                
                ex.printStackTrace();
            } catch (IOException ex1) {
                ex.printStackTrace();
            }
        }
    }
    /**
     * Classe interna que trata os eventos de solicitação do cliente.
     */
    private class EscutaCliente implements Runnable {

        // Lê as entradas do cliente.
        Scanner leitor;
        // Escreve informações para o cliente.
        PrintWriter escritor;

        public EscutaCliente(Socket socket) {
            try {

                // Pega o input do socket.
                leitor = new Scanner(socket.getInputStream());
                // Pega o output do socket.
                escritor = new PrintWriter(socket.getOutputStream());
                System.out.println("Criando leitor e escritor...");
            } catch (IOException ex) {
                leitor.close();
                escritor.close();
                
                ex.printStackTrace();
            }
        }

        // Fica executando infinitamente recebendo input e escrevendo output.
        @Override
        public void run() {

            String mensagem;
            try {
                while ((mensagem = leitor.nextLine()) != null) {
                    System.out.println("Mensagem: " + mensagem);
                    String termos[] = mensagem.split(";");

                    String acao = termos[0];

                    if (acao.equalsIgnoreCase("LOGIN")) {
                        if (login(termos[1], termos[2])) {
                            escritor.println("true");
                            escritor.flush();
                        } else {
                            escritor.println("false");
                            escritor.flush();
                        }
                    }

                    if (acao.equalsIgnoreCase("ADD_DOCUMENTO")) {
                        if (criarDocumento(termos[1])) {
                            escritor.println("true");
                            escritor.flush();
                        } else {
                            escritor.println("false");
                            escritor.flush();
                        }
                    }
                    
                    if (acao.equalsIgnoreCase("PEGAR_LOGIN")) {
                        String usuario = pegarLogin(termos[1]);
                        if (usuario != null) {
                            escritor.println(usuario);
                            escritor.flush();
                        } else {
                            escritor.println("NULO");
                            escritor.flush();
                        }
                    }

                    if (acao.equalsIgnoreCase("PEGAR_DOCUMENTOS")) {
                        List<String> usuarioDocumentos = pegarDocumentos(termos[1]);

                        for (String d : usuarioDocumentos) {
                            escritor.println(d);
                            escritor.flush();
                        }
                    }

                    if (acao.equalsIgnoreCase("CADASTRAR")) {
                        if (cadastrar(termos[1], termos[2])) {
                            escritor.println("true");
                            escritor.flush();
                        } else {
                            escritor.println("false");
                            escritor.flush();
                        }
                    }

                    if (acao.equalsIgnoreCase("COMPARTILHAR_DOCUMENTO")) {
                        // Termos[1] é o nome do documento "usuario-Documento_ESPACO_1"
                        // Separando no split
                        // termos[1]..split("-")[0] == usuario
                        // termos[1]..split("-")[1] == Documento_ESPACO_1

                        String[] termosCompartilhar = termos[1].split("-");

                        String usuario = termosCompartilhar[0];
                        String documento = termosCompartilhar[1];

                        if (compartilharDocumento(usuario, documento, termos[2])) {
                            escritor.println("true");
                            escritor.flush();
                        } else {
                            escritor.println("false");
                            escritor.flush();
                        }
                    }
                }
            } catch (NoSuchElementException ex) {
                leitor.close();
                escritor.close();

                System.out.println("Cliente desconectou do servidor!");
                ex.printStackTrace();
            } finally {
                leitor.close();
                escritor.close();
            }
        }

        private boolean login(String login, String senha) {
            for (Usuario u : usuarios) {
                if (u.getLogin().equalsIgnoreCase(login)) {
                    if (u.getSenha().equals(senha)) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
            return false;
        }

        private boolean criarDocumento(String nomeDocumento) {
            for (String d : documentos) {
                if (d.equalsIgnoreCase(nomeDocumento)) {
                    return false;
                }
            }
            documentos.add(nomeDocumento);

            try {
                Naming.rebind(nomeDocumento, new Texto());
            } catch (MalformedURLException | RemoteException ex) {
                ex.printStackTrace();
            }

            return true;
        }

        private List<String> pegarDocumentos(String usuario) {
            List<String> documentosUsuario = new ArrayList<String>();

            for (String d : documentos) {
                // Separa o nome do documento no "-", 
                //índice 0 com nome do usuario e índice 1 com o nome do documento.
                String[] documentoSplit = d.split("-");

                String usuarioDocumento = documentoSplit[0];

                System.out.println("PEGANDO DOCUMENTO " + d);

                if (usuarioDocumento.equalsIgnoreCase(usuario)) {
                    documentosUsuario.add(documentoSplit[1].replaceAll("_ESPACO_", " "));
                } else {
                    // Se o vetor do documento tiver mais de dois espaçoe é por que ele foi compartilhado.
                    if (documentoSplit.length > 2) {
                        String usuarioCompartilhado = documentoSplit[2];
                        if (usuarioCompartilhado.equalsIgnoreCase(usuario)) {
                            documentosUsuario.add("(Compartilhado) " + documentoSplit[1].replaceAll("_ESPACO_", " "));
                        }
                    }
                }
            }

            documentosUsuario.add("FIM");

            return documentosUsuario;
        }

        private boolean cadastrar(String login, String senha) {
            for (Usuario u : usuarios) {
                if (u.getLogin().equalsIgnoreCase(login)) {
                    return false;
                }
            }

            usuarios.add(new Usuario(login, senha));

            return true;
        }

        private boolean compartilharDocumento(String usuario, String documento, String usuarioCompartilhar) {
            for (String d : documentos) {
                // Separa o nome do documento no "-", 
                //índice 0 com nome do usuario e índice 1 com o nome do documento.
                String[] documentoSplit = d.split("-");

                // Separando o documento de nome "login-Documento_ESPACO_1" no traço
                String usuarioDocumento = documentoSplit[0]; // igual a login
                String nomeDocumento = documentoSplit[1]; // igual a Documento_ESPACO_1

                System.out.println("DOCUMENTO COMPARTILHAMENTO " + d);

                /*  Verifica se o usuário que deseja compartilhar 
                 é o dono do documento, e depois verifica se o documento
                 tem o mesmo nome do documento que será compartilhado.
                
                 Neste caso é concatenado ao nome do documento o um ";"  
                 e o nome do usuário que irá receber o compartilhamento.
                 */
                if (usuarioDocumento.equalsIgnoreCase(usuario)) {
                    if (nomeDocumento.equalsIgnoreCase(documento)) {
                        int indice = documentos.indexOf(d);

                        System.out.println("INDICE DO DOCUMENTO " + indice);

                        d += "-" + usuarioCompartilhar;

                        documentos.add(indice, d);

                        return true;
                    }
                }
            }

            return false;
        }

        private String pegarLogin(String documento) {
            System.out.println("NOME DO DOCUMENTO " +documento);
            for (String d : documentos) {
                // Separa o nome do documento no "-", 
                //índice 0 com nome do usuario e índice 1 com o nome do documento.
                String[] documentoSplit = d.split("-");

                // Separando o documento de nome "login-Documento_ESPACO_1" no traço
                String usuarioDocumento = documentoSplit[0]; // igual a login
                String nomeDocumento = documentoSplit[1]; // igual a Documento_ESPACO_1
                
                System.out.println("DOCUMENTO COMPARTILHADO " + d);
                
                if (nomeDocumento.equalsIgnoreCase(documento)) {
                    return usuarioDocumento;
                }
            }
            return null;
        }
    }

    public static void main(String[] args) {
        new DeskDocServidor();
    }
}
