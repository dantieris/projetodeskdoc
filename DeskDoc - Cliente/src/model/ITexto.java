package model;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ITexto extends Remote {
    public void setTexto(String texto) throws RemoteException;
    public String getTexto() throws RemoteException;
}
